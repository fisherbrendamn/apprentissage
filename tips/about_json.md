# A propos de JSON et de python

Dans les bonus du cours python d'OpenClassRoom, on nous dit qu'on peut stocker nos listes / dictionnaires dans un fichier json.
Ainsi, après avoir translaté les données en json, on doit importer les bibliothèques qui nous permettront de modifier du json.

<br>

## Convertir de json à python - Load

Nos données sont dans un fichier json et on souhaite s'en servir dans notre code Python.

```python
import json

# some JSON:
x = '{ "name":"John", "age":30, "city":"New York"}'

# parse x:
y = json.loads(x)

# the result is a Python dictionary:
print(y["age"])
```

<br>

## Convertir de python à json - Dumps

```python
import json

# a Python object (dict):
x = {
  "name": "John",
  "age": 30,
  "city": "New York"
}

# convert into JSON:
y = json.dumps(x)

# the result is a JSON string:
print(y)
```

