#!/usr/bin/python3

import socketserver
import threading
import pprint

LEVEL={}
LEVEL[1] = 17
LEVEL[2] = 21
LEVEL[3] = 25

MAPPING={}
MAPPING[0] = 3
MAPPING[3] = 2
MAPPING[2] = 1

class Board:
    """Une representation du plateau"""
    def __init__(self, number):
        self.__number = number
        self.__total_number = number

    def display(self):
        return '|' * self.__number + '.' * (self.__total_number - self.__number)

    def number(self):
        return self.__number

    def run(self, number):
        self.__number -= number

class Game(socketserver.StreamRequestHandler):
    """Une partie"""

    def play(self, level):
        self.board = Board(LEVEL[self.level])
        while self.board.number() > 1:
            baton = 0
            while baton not in [1, 2, 3]:
                self.request.sendall(bytes(self.board.display(), 'ascii'))
                self.request.sendall(b"\nHow many baton to remove (1, 2 or 3) : ")
                baton = int(self.rfile.readline().strip())
            self.board.run(baton)
            self.request.sendall(bytes(self.board.display(), 'ascii'))
            self.request.sendall(bytes("\nI removed {} baton(s) !\n".format(MAPPING[self.board.number() % 4]), 'ascii'))
            self.board.run(MAPPING[self.board.number() % 4])
        self.request.sendall(b"\nYou lose looser ! HAHAHA !!")

    def handle(self):
        self.level = 0
        while self.level not in [1, 2, 3]:
            self.request.sendall(b"Welcome on Batons Game !\n\n")
            self.request.sendall(b"Loosers 17 batons: 1\n")
            self.request.sendall(b"Loosers 21 batons: 2\n")
            self.request.sendall(b"Loosers 25 batons: 3\n\n")
            self.request.sendall(b"Choose your level : ")
            self.level = int(self.rfile.readline().strip())
        print("{} choose level {}".format(self.client_address[0], self.level))
        self.request.sendall(bytes("Let's go with {} batons you looser !\n".format(LEVEL[self.level]), 'ascii'))
        self.play(self.level)

class ThreadedTCPServer(socketserver.ThreadingMixIn, socketserver.TCPServer):
    pass

if __name__ == "__main__":
    HOST, PORT = "0.0.0.0", 2323

    server = ThreadedTCPServer((HOST, PORT), Game)
    with server:
        ip, port = server.server_address

        # Start a thread with the server -- that thread will then start one
        # more thread for each request
        server_thread = threading.Thread(target=server.serve_forever)
        # Exit the server thread when the main thread terminates
        server_thread.daemon = True
        server_thread.start()
        print("Server loop running in thread:", server_thread.name)

        while True:
            pass

        server.shutdown()