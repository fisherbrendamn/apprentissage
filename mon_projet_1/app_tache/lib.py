import logging
import os
import json

from constants import DATA_DIR

LOGGER = logging.getLogger()

class Liste(list): # Permet d'hériter de tout ce qui est dans la classe (list)
    def __init__(self, nom):
        self.nom = nom

    
    def ajouter(self, element):
        """isistance permet de vérifier si un élément est d'un certain type"""
        if not isinstance(element, str):            
            raise ValueError("Vous ne pouvez ajouter que des chaines de caractères")

        """Si l'élement est déjà dans self / dans la liste
           On logg cette erreur et on sort de la méthode"""
        if element in self:
            LOGGER.error(f"{element} est déjà dans la liste.")
            return False

        self.append(element)
        return True

    
    def enlever(self, element):
        if element in self:
            self.remove(element)
            return True
        else:
            return False

    
    def afficher(self):
        print(f"Ma liste de {self.nom} : ")
        for element in self:
            print(f" - {element}")

    
    def sauvegarder(self):
        chemin = os.path.join(DATA_DIR, f"{self.nom}.json")
        """On sauvegarde dans un fichier json qui va se trouver en bout de chemin et qui portera le nom de notre instance"""
        if not os.path.exists(DATA_DIR):
            os.makedirs(DATA_DIR)
        
        with open(chemin, 'w') as f:
            json.dump(self, f, indent=4)

        return True

if __name__ == "__main__":
    liste = Liste("carrefour")
