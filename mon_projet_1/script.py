# Les classes
# Exmeple de la voiture, on va créer une classe avec un objet voiture contenant différents attributs

class Voiture:

    def __init__(marque):
        marque = marque

        
print(Voiture.marque)
print(Voiture.couleur)

# Les instances

voiture_01 = Voiture()

print(voiture_01.marque.lower())

Voiture.marque = "Lancia" # Attribut de classe
voiture_01.marque = "Renaut"  # Attribut de l'instance

print(voiture_01.marque, Voiture.marque)

