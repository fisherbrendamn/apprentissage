class Voiture:
    voiture_crees = 0

    def __init__(self, marque):
        Voiture.voiture_crees += 1
        self.marque = marque

    def afficher_marque(self):
        print(f"La voiture est une {self.marque}")



voiture_01 = Voiture("Peugeot")
voiture_02 = Voiture("Lancia")

Voiture.afficher_marque(voiture_01)
voiture_01.afficher_marque() # même chose

# print(voiture_01.marque)
# print(voiture_02.marque)
# print(Voiture.voiture_crees)