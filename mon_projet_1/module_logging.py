import logging

logging.basicConfig(level=logging.DEBUG,
                    filename="app.log",
                    filemode='a',
                    format='%(asctime)s - %(levelname)s - %(message)s') 
"""On fixe le niveau minimal de loggin | Ici, le DEBUG est une constante du module logging
   Avec format, on change l'affichage des log | ici, on dit qu'on veut l'heure, le level, et le message"""

logging.debug("La fonction a bien été exécutée")
logging.info("Message d'information général")
logging.warning("Attention !")
logging.error("Une erreur est survenue")
logging.critical("Erreur critique")

"""Écrire dans un fichier de log"""

