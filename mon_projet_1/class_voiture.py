class Voiture:
    
    #essence = 100

    def __init__(self):
        self.essence = 100

    def afficher_reservoir(self):
        print(f"La voiture contient {self.essence}L d'essence.")

    def roule(self, km):
        #self.km = km
        if self.essence <= 0:
            print("Vous n'avez plus d'essence, faites le plein.")
            return                                                  # Le return permet de sortir de la méthode. Si on a plus d'essence, on empêche de rouler, on sort, donc pour faire le plein.

        distance = (km * 5) / 100
        self.essence -= distance

        if self.essence < 10:
            print("Vous n'avez plus beaucoup d'essence, faites le plein.")
            
            
        #return distance

    def faire_le_plein(self):
        self.essence_restant = 100 - self.essence 
        self.essence += self.essence_restant
        
        print("Vous faites le plein ...")
        self.afficher_reservoir()
        
        print("Vous pouvez repartir !")
        self.afficher_reservoir() 


# v = Voiture()

# while True:
#     pass

# v.roule(1600)
# v.afficher_reservoir()
# v.faire_le_plein()



    # def essence(self, essence):
    #     self.essence = essence