class Vehicule:
    def avancer(self):
        print("Le véhicule démarre")


class Voiture(Vehicule):
    def avance(self):
        super().avancer()
        print("La voiture roule")


class Avion(Vehicule):
    def avance(self):
        super().avancer()
        print("L'avion vole")


