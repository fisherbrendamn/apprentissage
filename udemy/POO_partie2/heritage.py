

projets = ["pr_GameOfThrones", "HarryPotter", "pr_Asterix", "classe_Dorcel"]

class Utilisateur:
    def __init__(self, nom, prenom):
        self.nom = nom
        self.prenom = prenom


    def __str__(self) -> str:
        return f"utilisateur {self.prenom} {self.nom} : {self.__class__}"

    
    def afficher_projets(self):
        for projet in projets:
            if not projet.startswith("classe_"):
                print(projet)



class Junior(Utilisateur):
    def __init__(self, nom, prenom):
        #Utilisateur.__init__(self, nom, prenom)
        super().__init__(nom, prenom)
    

    def afficher_projets(self):
        for projet in projets:
            if not projet.startswith('pr_'):
                print(projet)
                

class Senior(Utilisateur):
    def __init__(self, nom, prenom):
        super().__init__(nom, prenom)


    def afficher_projets(self):
        for projet in projets:
            print(projet)
        #return super().afficher_projets()



harald = Utilisateur("Maldant", "Harald")
print(harald)
harald.afficher_projets()
        
marcelin = Junior("Saint", "Marcelin")
print(marcelin)
marcelin.afficher_projets()

maxime = Senior("Brun", "Maxime")
print(maxime)
maxime.afficher_projets()