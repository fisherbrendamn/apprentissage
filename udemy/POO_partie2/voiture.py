class Voiture:

    voitures_crees = 0

    def __init__(self, marque, vitesse, prix):
        Voiture.voitures_crees += 1
        self.marque = marque
        self.vitesse = vitesse
        self.prix = prix
        
    
    def __str__(self):
        return f"Voiture de marque {self.marque} avec vitesse maximale de {self.vitesse} pour le prix de {self.prix}"

    
    @classmethod
    def peugeot(cls):
        return cls(marque="Peugeot", vitesse=200, prix=10_000)


    @classmethod
    def alpine(cls):
        return cls(marque="Alpine", vitesse=275, prix=80_000)


    """Méthode statique"""
    @staticmethod
    def afficher_nombre_voiture():
        print(f"Vous avez {Voiture.voitures_crees} dans votre garage.")
    

# class Garage:

#     def __init__(self, nom):
#         self.garage = []
#         self.nom = nom


#     def ajouter_voiture(self, voiture):
#         self.voiture = voiture
#         self.garage.append(voiture)


#     def afficher_garage(self):
#         print(f"Dans le garage {} il y a {Voiture.afficher_nombre_voiture}")
        



mavoiture = Voiture.peugeot()
alpine = Voiture.alpine()


print(f"Prix : {mavoiture.prix}, Vitesse maximale : {mavoiture.vitesse}km/h, Marque : {mavoiture.marque}")
print(alpine)
affichage = str(alpine)
print(affichage)
Voiture.afficher_nombre_voiture()
