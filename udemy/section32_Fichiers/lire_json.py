import json

chemin = '/Users/hmaldant/Documents/sandbox/apprentissage/udemy/section32_Fichiers/config.json'

with open(chemin, 'w') as fjson:
    json.dump(list(range(10)), fjson, indent=4)


# Lire

with open(chemin, 'r') as f:
    liste = json.load(f)
    print(liste)