pathf = '/Users/hmaldant/Documents/sandbox/apprentissage/udemy/section32_Fichiers/donnees.txt'

with open(pathf, 'r') as f:
    content = repr(f.read()) # représentation en chaine de caractère
    print(content)

with open(pathf, 'r') as f:
    content = f.readlines() # Liste contenant chaque ligne comme élément
    print(content)

with open(pathf, 'r') as f:
    content = f.read().splitlines() # représentation en chaine de caractère en enlevant les caractères spéciaux
    print(content)
