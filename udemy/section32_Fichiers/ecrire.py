pathf = '/Users/hmaldant/Documents/sandbox/apprentissage/udemy/section32_Fichiers/donnees.txt'

with open(pathf, 'w') as f:
    f.write("Bonjour aussi")

# On peut aussi utiliser un autre mode, 'a' pour append, équivaut à >> fichier.txt

with open(pathf, 'a') as f:
    f.write("\nComment allez-vous aujourd'hui ?")


with open(pathf, 'r') as f:
    content = f.read() # représentation en chaine de caractère
    print(content)