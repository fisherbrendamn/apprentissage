liste = [-5, -4, -3, -2, -1, 0, 1, 2, 3, 4, 5]

nombres_positifs = []
for i in liste:
    if i > 0:
        nombres_positifs.append(i)
print(nombres_positifs)

nombres_negatifs = [i for i in liste if i < 0]
print(nombres_negatifs)

nombres_positifs_par_2 = [i*2 for i in liste if i > 0]
print(nombres_positifs_par_2)