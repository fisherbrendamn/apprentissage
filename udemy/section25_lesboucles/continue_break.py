liste = ['1', '2', 'bonjour', '44', "Harald MALDANT"]
for element in liste:
    if element.isdigit():
        continue # Si l'élément est un digit, continue va repasser directement dans la boucle for et ne pas exécuter le print(element)
    print(element)


for element in liste:
    if element.isdigit():
        break # Dans ce cas de figure, 1 étant un digit, on sort directement de la boucle et on affiche rien 
    print(element)