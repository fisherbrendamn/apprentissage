nombre = range(-10,11)
nombre_positif = [i for i in nombre if i > 0]

print(nombre_positif)

nombre = range(-4, 5)
nombre_pair = [i for i in nombre if i % 2 == 0]

print(nombre_pair)

nombre = range(10)
nombre_inverse = [i if i % 2 == 0 else -1 for i in nombre]
print(nombre_inverse)