age = 20
if age > 18:
    print('\nVous êtes majeur.')

# AND
user = 'admin'
password = 'admin'

if user == 'admin' and password == 'admin':
    print("Accès autorisé")

# OR
prenom = 'harald'
nom = 'maldant'

if prenom == 'harald' or nom == 'maldant':
    print('Accès autorisé, vous êtes de la même famille')

# NOT
if not user == 'admin':
    print('Accès refusé')