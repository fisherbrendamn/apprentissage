import os

def verifier_fichier():
    if os.path.exists("fichier.txt"):
        return True
    return False

# Dès qu'on croise un return, on sort de la fonction

# Valeur par défaut d'un paramètre

def afficher(message="Message par defaut"):
    print(message)

afficher()
    
# Espace global et local

a = 5
b = 10 

def addition():
    global a
    a += 5
    print(a)

addition()