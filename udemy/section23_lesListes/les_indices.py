# On commence à compter à partir de 0
liste = [1,2,3,4,'cent cinquante']
print(liste[4])

# On peut aussi utiliser des indices négatifs pour commencer à partir de la fin
print(liste[-1])

# Plusieurs indices
ma_liste = [1,2,['Hallo','Maman'],5,7]
print(ma_liste[2][0])