# Ajouter un élément .append
# .append est une méthode, c'est à dire qu'elle s'applique à un objet
# Ici nous avons une liste, un objet liste même, donc nous pouvons utiliser une méthode qui s'applique sur les objets de type list

# Pour ajouter plusieurs élements d'un coup, on utilise la méthode .extend()
liste = [1, 2, 3, 4, 'caca']
print('Contenu initial de ma liste : ', liste)

liste.append(100)
print('Ajout de contenu dans ma liste : ', liste)

liste.extend([99, 'Harald', True]) # On passe une liste dans .extend
print('Et voici ma liste étendue : ', liste)

# Pour enlever un élément
liste.remove(100) # On enlève uniquement la première occurrence
print('On a enlevé le nombre 100 : ',liste)