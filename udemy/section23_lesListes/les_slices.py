# Des tranches de notre liste

liste = ['Utilisateur_01',
         'Utilisateur_02',
         'Utilisateur_03',
         'Utilisateur_04',
         'Utilisateur_05',
         'Utilisateur_06']

print(liste[0:2]) # Le deuxième élément est exclusif
print(liste[:-1]) # On récupère tout sauf le dernier
print(liste[0:]) # On récupère toute la liste
print(liste[:]) # On récupère toute la liste
print(liste[1::2]) # On récupère un élément sur 2, pas de 2
print(liste[1:-2:2]) # On récupère les utilisateurs 2 par 2 à partir de 1 jusqu'à l'avant-dernier, donc -2, avec un pas de 2
print(liste[::-1]) # On inverse la liste

print(liste[-4:0])
print(liste[1:5])
print(liste[3:])
print(liste[::])

premier_dernier = liste[::len(liste)-1]
print(premier_dernier)