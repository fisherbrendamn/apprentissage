# C'est comme une liste sauf qu'on ne peut pas rajouter ou enlever d'élements
# Pour définir un tuple, c'est comme pour les listes sauf qu'on met des () au lieu des []
mon_tuple = (1, 2, 3)
print(mon_tuple)

print(mon_tuple.count(1)) # On peut intéragir avec un tuple
# On peut aussi convertir un tuple en liste
ma_liste_from_tuple = list(mon_tuple)
ma_liste_from_tuple.append('prout')
print(ma_liste_from_tuple)

# Puis reconvertir en tuple
mon_tuple = tuple(ma_liste_from_tuple)
print('Ceci est mon tuple : ',mon_tuple)
