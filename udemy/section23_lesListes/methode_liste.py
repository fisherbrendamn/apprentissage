employes = ["Harald", "Max", "Mathias", "Adrien", "Georges", "Morgan", "Zemmour"]
print(employes.index("Max"))

print(employes.count('Max'))

# employes.sort() <-- modifie la liste
# print(employes)

employes_sorted = sorted(employes)
print("Liste triées : ",employes_sorted)

employes_sorted.reverse()
print('Liste triées inversées : ',employes_sorted)
print("""

    """)

# Enlever en fonction de l'index
employes = ["Harald", "Max", "Mathias", "Adrien", "Georges", "Morgan", "Zemmour"]
# Disons que je veux enlever le dernier arrivé
element = employes.pop(-1) # Ou ne rien mettre
print("Dernier élement retiré : ",element)
print("Liste mise à jour : ",employes)


# Enlever tout le monde
employes.clear()
print(employes)
# Si je veux retourner None au lieu de la liste vide, j'assigne à une variable la liste soumise à la methode clear()