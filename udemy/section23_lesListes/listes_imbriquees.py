liste = ['Python', ['Java', 'JS', 'C++', ['C'], ['Ruby', 'Ruby On Rails'], 'GO', 'RUST']]

# ['Ruby', 'Ruby On Rails']

print(liste[1][4][0]) # Faire les choses étape par étape

# On peut également récupérer une lettre dans un index de chaine
# Je veux le v de Java

print(liste[1][0][2])
print(liste[1][4][1][::-1])