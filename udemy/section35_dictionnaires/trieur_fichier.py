import os
import glob
from shutil import move
import re
import shutil

le_path = "/Users/hmaldant/Documents/sandbox/apprentissage/udemy/section35_dictionnaires/01-sources/tri_fichiers_sources/"
chemin = os.path.join(le_path)

# Extension
pdf = le_path + '*.pdf'
mp3 = le_path + '*.mp3' 
mp4 = le_path + '*.mp4' 
tous = le_path + '*' 

# Création des répertoires
dir_to_create = ['Documents','Musique','Vidéos','Images']
for dir in dir_to_create:
    if (os.path.exists(le_path + dir)): # On vérifie si le dossier existe déjà
        continue
    else:
        os.mkdir(le_path + dir)

# print(chemin)
fichiers_a_trier = glob.glob(tous, recursive=True)

for file in fichiers_a_trier:
    x = re.search('jpe?g', file)
    y = re.search('mp[3-4]|wav', file)
    if x:
        move(file, le_path + 'Images')
        print(file)
    elif y:
        move(file, le_path + 'Musique')
        print(file)
    #print(file)

