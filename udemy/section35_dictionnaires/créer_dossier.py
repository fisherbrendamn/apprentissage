import os

chemin = "/Users/hmaldant/Documents/sandbox/apprentissage/section35_dictionnaires/dossiers"

d = {"Films": ["Le Seigneur des Anneaux",
               "Harry Potter",
               "Moon",
               "Forest Gump"],
    "Employes": ["Harald",
                 "Beltram",
                 "Nadine"],
    "Exercices" : ["Python3.9",
                   "Les variables",
                   "Les objets"]}

for cle, valeur in d.items():
    for dossier in valeur:
        chemin_dossier = os.path.join(chemin, cle, dossier)
        print(chemin_dossier)
        os.makedirs(chemin_dossier, exist_ok=True)