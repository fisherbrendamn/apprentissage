# prenoms = " .Harald, beltram nadine"
# prenoms_clean = prenoms.strip(" ., ")
# print(prenoms_clean)

from pprint import pprint

chemin_fichier = '/Users/hmaldant/Documents/sandbox/apprentissage/udemy/section35_dictionnaires/prenoms.txt'

liste = []

with open(chemin_fichier, 'r') as f:
    lines = f.read().splitlines() # On casse le str en liste
pprint(lines)

prenoms = []

for line in lines:
    prenoms.extend(line.split()) # Extend permet d'ajouter plusieurs valeurs à la liste

prenoms_final = [prenom.strip(',. ') for prenom in prenoms] # Compréhension de liste
pprint(prenoms_final)

with open(chemin_fichier, 'w') as f:
    f.write("\n".join(sorted(prenoms_final))) # On trie prenoms_final au moment d'écrire dans le fichier 
# pprint(prenoms)