dico = {"prenom" : "Harald",
        "Profession" : "AdminSys",
        "Ville" : "Guéret"}

print(1)
print(dico.keys())
print(dico.values())

print(2)
for cle in dico:
    print(cle, dico[cle])

print(3)
print(dico.items())

print(4)
for cle, valeur in dico.items():
    print(cle, valeur)