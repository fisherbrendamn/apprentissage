dico = {
    0: {"prenom":"Harald","profession":"Adminsys",
        "ville":"Pornic"},
    1: {"prenom":"Beltram","profession":"Développeur",
        "ville":"Nantes"},
    19: {"prenom":"Mathias","profession":"Développeur",
        "ville":"Pornic"}
}

# print(dico[19]["prenom"])

# print(dico.get("prenom", "Key does not exist"))

films = {
    "Le Seigneur des Anneaux" : 12,
    "Harry Potter" : 19,
    "Blade Runner" : 11.5
    }


prix = 0

def afficher_prix ():
    for key in films:
        prix = films[key]
        print(f'{key} = {prix} euro')

films['Harry Potter'] = "17.5" # Modification

# Ajout clé
films['Fight Club'] = 14
films['Drive'] = 14

# Supprimer clé
if 'Drive' in films:
    del films['Drive']
else:
    print("Le film n'est pas présent dans le dictionnaire")


afficher_prix()



