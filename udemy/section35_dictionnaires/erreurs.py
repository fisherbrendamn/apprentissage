# LBYL

liste = [2, 5, "prout", 9]

for i in liste:
    if not str(i).isdigit():
        liste.remove(i)

total = sum(liste)
print(total)

# EAFP

try:
    total = sum(liste)
except:
    total = 0

print(total)

#

a = 5
b = 10

try:
    resultat = a / b
except ZeroDivisionError:
    print("Division par zéro impossible")
except TypeError:
    print("La variable b n'est pas du bon type")
except NameError as e:
    print("[ERROR]", e)
else:
    print(resultat)
finally:
    print("Fin du bloc")