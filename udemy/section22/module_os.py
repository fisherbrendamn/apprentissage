import os

chemin = '/Users/hmaldant/Documents/sandbox/apprentissage'

# Je veux créer un dossier
dossier = os.path.join(chemin, 'dossier', 'formation') # Permet de joindre (comme un coup de fil) le chemin et de spécifier le dossier
print(dossier)

# Je crée le dossier
# On peut vérifier avec ue structure conditionnelle si le dossier existe (avec la methode exists())
if not os.path.exists(dossier):
    os.makedirs(dossier)
else:
    print('Le dossier existe déjà, rien à faire.')

# Sinon
os.makedirs(dossier, exist_ok=True)

# remove
if os.path.exists(dossier):
    os.removedirs(dossier)
else:
    print("Le dossier n'existe pas")