import random

# Randint
r = random.randint(0, 10) # La seconde valeur est incluse
print(r)

# Uniform
r = random.uniform(0, 10) # La seconde valeur est incluse
print(r)

# Randrange
r = random.randrange(10) # La valeur est exclusive
print(r)
# Avec un pas de 10 par exemple
r = random.randrange(0, 101, 10) # Entre 0 et 100 avec un pas de 10
print(r)
