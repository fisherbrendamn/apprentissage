import random
import math

print('Bonjour le monde')

liste = []
liste.append("Bonjour")

print(liste)

# Chaine de caractères brute
# RAW STRING

print("c:\dossiers\thibault\nouveautés") # <-- Ne vas pas fonctioner \t pour tabulation et \n pour saut de ligne
print(r"c:\dossiers\thibault\nouveautés") # le r devant la chaine permet de signifier que celle-ci est brut

# Caractère spéciaux interprétés par python
# \a caractère d'appel (BEL)
# \b caractère de retour arrière
# \f saut de page
# \n retour à la ligne
# \r retour chariot
# \t tabulation horizontale
# \v tabulation verticale

# Les nombres

100000
# = 
1_000_000

# Les booléens
# Tous les objets peuvent être vrais ou faux, avec la fonction bool 
bool("Bonjour") # True
# Une chaine de caractère contenant au moins 1 caractère est vrai, sinon elle est fausse
# Pour les nombres, il n'y a que 0 qui est considéré comme False
# pour les dico, c'est false quand ils sont vides

# Classe de type
str()
int()
float()
bool()
# On peut utiliser ces méthodes pour convertir les objets d'un type vers un autre type
a = 5
str(a) # --> "5"
bonjour = True

# Affectations
# Simple
a = "bonjour"
b = "Au revoir"
# Paralleles
a,b = "bonjour","au revoir" 
# Avantage = inverser les valeurs contenues dans deux variables
# Exemple
a,b = b,a
a,b,c,d = 3,55,13313,8
# Affectation multiple 
# plutot que de faire 
a,b,c = 5,5,5
# On peut faire 
a = b = c = 5

# SingleTon et small integer caching


# Manipuler les chaines de caractères
# La méthode replace

phrase = "Bonjour tout le monde"
phrase_replace = phrase.replace('tout le monde', 'à toutes et à tous').replace('jour','soir')

# La méthode strip

"  bonjour  ".strip(' ujor') # Va enlever les espaces et les caractères spécifiés qu'il rencontre

# La méthode rstrip et lstrip (right ou left)

"  bonjour  ".lstrip(" ujor")
"  bonjour  ".rstrip(" ujor")

# Séparer et joindre

"1, 2, 3, 4, 5, 12".split(", ")
['1', '2', '3', '4', '5', '12']

", ".join("1, 2, 3, 4, 5, 12".split(", "))
'1, 2, 3, 4, 5, 12' # Avec cette méthode, on retombe sur nos pieds.
# On peut vouloir faire ça pour extraire les nombres dans un premier temps, puis les modifier, puis les remettre dans une variable

# remplir de zero avec .zfill()
"9".zfill(4)
# >>> '0009'

for i in range (100+1):
    print(str(i).zfill(3))


# La méthode 'is'
# V&rifier que la chaine ne contient que des minuscules
"bonjour".islower()
#>>> True
# is title
"Bonjour Tout Le Monde".istitle()
#>>> True
"bonjour".isdigit()

# Compter les occurrences
# Méthode count
"Bonjour le jour".count(" jour") # 1

# Trouver une chaine
# Méthode find et index

"bonjour le jour".find(" jour")
# Il existe aussi .rfind()

# La seule différence entre les deux est que find retourne -1 si elle ne trouve pas, tandis que index envoie une erreur

# Chercher au début et à la fin
# Par exemple vérifier l'extension d'un fichier
"image.png".endswith('.png')
"image.p,g".startswith("image")

# Les opérateurs mathématiques
print(10 // 2) # Division entière
print(2 ** 4) # Puissance

# Opérateur d'assignation
i = i + 1
i += 1 # Pareil 
i -= 1 
i *= 1
i /= 1
i %= 1
i //= 1
i **= 1

# Exemple
i = 5 
i += 10
print(i) # 15

# Opérateurs de comparaison
i == 10 # égal
i != 10 # Différent de 

# Formatage de texte
prenom = "harald"
print(f"bonjour {prenom.capitalize()} !")
# Exemple
proto = "https://"
site_name = "Docstring"
extension = "fr"

url = f"{proto}www.{site_name}.{extension}"
print(url.lower())

# Avant 3.6
age = 27
phrase_test = "J'ai {0} ans, {0} ce n'est pas très".format(age)
print(phrase_test)
# Avantage des formats
# Pratique quand on a pas encore définit les variables qui seront présentes dans la chaine de caractères
# Exemple qui marche pas 
from constant import BONJOUR
def get_weekly_progress(user):
    print("progression")
    return prout


user = input("Entrez votre nom d'utilisateur : ")
progression = get_weekly_progress(user) # Cette fonction n'existe pas, c'est un exemple

welcome_msg = BONJOUR.format(prenom=user, nmbre_videos=progression)
print(welcome_msg)

prenom = "Harald"
phrase = f"Bonjour {prenom}, cette semaine vous avez regardé {nmbre_video} vidéos." # nmbre_video n'est pas définit

