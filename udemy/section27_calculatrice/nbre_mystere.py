import random
import sys

def essai_restant():
    print(f"Il te reste {NBRE_ESSAI} essais.")

NBRE_ESSAI = 5

print("*** Le jeu du nombre mystère ***")

for i in range(NBRE_ESSAI+1):
    essai_restant()
    nombre_mystere = random.randint(1,101)
    guess = int(input("Devine le nombre mystère : "))
    while not guess in range(0,101):
        print("Veuillez entrer un nombre valide")
        essai_restant()
        guess = int(input("Devine le nombre mystère"))
        sys.exit()
    if guess == nombre_mystere:
        print(f"Félicitation, vous avez trouvé le nombre mystère qui était : {nombre_mystere}.")
        print("FIN DE LA PARTIE -------")
    elif guess > nombre_mystere:
        print(f"Le nombre mystère est plus petit que {guess}.")
    elif guess < nombre_mystere:
        print(f"Le nombre mystère est plus grand que {guess}.")
    NBRE_ESSAI -= 1
    print(f"Controle : i = {i}")


