# while True:
#     nombre_1 = input("Entrez un permier nombre : ")
#     nombre_2 = input('Entrez un deuxième nombre : ')
#     if not nombre_1.isdigit() or not nombre_2.isdigit():
#         print('Veuillez entrer deux nombres valides.')
#     else:
#         print(f"Le résultat de l'addition de {nombre_1} par {nombre_2} est {int(nombre_1) + int(nombre_2)}.")
#         break

# Autre solution du correcteur

a = b = ""

while not (a.isdigit() and b.isdigit()):
    a = input("Entrez un premier nombre : ")
    b = input("Entrez un second nombre : ")
    if not (a.isdigit() and b.isdigit()):
        print('Veuillez entrer deux nombres valides.')

print(f"Le résultat de l'addition de {a} par {b} est {int(a) + int(b)}.")
