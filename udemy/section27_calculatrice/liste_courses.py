import time

choix = ''
liste = []
nombres_interaction = 0

def spliter_intarct():
    print("-" * 25 + " " + str(nombres_interaction) + " " + "-" * 25)


def menu():
    print(
f"""
Choisissez parmi les 5 options suivantes : 

1: Ajouter un élément à la liste
2: Retirer un élément de la liste
3: Afficher la liste
4: Vider la liste
5: Quitter 
👉 Votre choix : """)


while True:
    (menu())
    nombres_interaction += 1 
    choix = input("\nChoix : ")
    spliter_intarct()
    if choix == '1': # Ajouter élément
        item = input("\tEntrez le nom de l'élément à ajouter : ")
        liste.append(item)
        print(f"\tL'élément {item} a bien été ajouté à la liste de course.")

    elif choix == '2': # retirer élément
        item = input(f"""Entrez le nom de l'élément que vous souhaitez supprimer parmis cette liste : {str(liste)}
        Élément à retirer : """)
        if item in liste:
            liste.remove(item)
            print(f"L'élélement {item} a bien été retiré de la liste de course.")
        else:
            print("Cet élément n'était pas présent dans la liste.")

    elif choix == '3':
        if liste:
            print("Voici le contenu de la liste : \n")
            for i, item in enumerate(liste, 1): # i permet d'avoir l'indice, item l'élément, ensuite la fonction enumerate va chercher dans la liste l'index et son élément. On dit également à la fonction de commencer à l'indice 1 au niveau affichage
                print(f"{i}. {item}")
        else:
            print("Votre liste ne contient aucun élément.")
    # elif choix == '3': # Afficher liste
    #     if not len(liste) == 0:
    #         print('Votre liste de course : ',liste)
    #         afficher_dernier_element = input("Voulez-vous afficher le dernier élément que vous avez ajouté ? (oui/non) ")
    #         if afficher_dernier_element == 'oui'.lower():
    #             print(f"Dernier élément ajouté à la liste : {liste[-1]}")
    #         else:
    #             print("Retour au menu.")
    #             time.sleep(0.5)
    #     else:
    #         print("Votre liste est vide.")

    elif choix == '4': # PURGE
        purge_liste = input(print("Souhaitez-vous vider la liste de courses ? (oui/non) "))
        if not purge_liste.lower() == 'oui':
            print("La liste n'a pas été vidée, retour au menu.\n")
        else:
            liste.clear()
            print("La liste a été purgée.")

    elif choix == '5': # QUITTER
        print("Sortie de la liste de courses, bonne journée !")
        time.sleep(1)
        break

    else:
        print("Nous n'avons pas compris votre choix, veuillez réessayer ...")
        time.sleep(0.5)