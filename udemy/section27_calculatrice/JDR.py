import random
from sys import exit

joueur_pts_de_vie = monstre_pts_de_vie =  50
joueur_potion_possede = 3

def potion_rend():
    potion_pts_rendu = random.randint(15, 51)
    return potion_pts_rendu


def intro_du_jeu():
    print(f"""
    Bienvenu dans ce RPG {joueur_nom}
    🤍 Vous possédez {joueur_pts_de_vie} points de vie
    🧪 Et vous avez en votre possession {joueur_potion_possede} potions de santé""")
    print("-" * 100)
    print("Un monstre surgit devant vous ! ")


def joueur_attaque():
    nombre_degat = random.randint(5,11)
    return nombre_degat


def monstre_attaque():
    nombre_degat = random.randint(5,16)
    return nombre_degat


joueur_nom = input("Quel est votre nom chevalier ? : ")
intro_du_jeu()

while joueur_pts_de_vie > 0 and monstre_pts_de_vie > 0:
    choix = input("Souhaitez-vous attaquer (1) ou utiliser une potion (2) ?")
    if choix == '1':
        j_attaque = joueur_attaque()
        monstre_pts_de_vie -= j_attaque
        print(f"""
        Vous attaquez le monstre et lui infligez {j_attaque} points de dégats !
        Points de vie du monstre : {monstre_pts_de_vie}
        """)
        m_attaque = monstre_attaque()
        joueur_pts_de_vie -= m_attaque
        print(f"""
        L'ennemi vous attaque à son tour et vous inflige {m_attaque} points de dégats ! 
        Il vous reste {joueur_pts_de_vie} points de vie
        """)
        print('-' * 100)
    if choix == '2':
        if joueur_potion_possede > 0:
            joueur_potion_possede -= 1
            pv_obtenues = potion_rend()
            joueur_pts_de_vie += pv_obtenues
            print(f"""
            Vous récupérez {pv_obtenues} points de vie ❤️ ({joueur_potion_possede} 🧪 restantes)
            """)
            m_attaque = monstre_attaque()
            joueur_pts_de_vie -= m_attaque
            print(f"L'ennemi vous a infligé {m_attaque} points de dégats")
            print(f"Il vous reste {joueur_pts_de_vie} points de vie")
            print('-' * 100)
        else:
            print("Vous n'avez plus de potions")
            m_attaque = monstre_attaque()
            joueur_pts_de_vie -= m_attaque
            print(f"L'ennemi vous a infligé {m_attaque} points de dégats")
            print(f"Il vous reste {joueur_pts_de_vie} points de vie")
            print('-' * 100)
    elif not choix.isdigit():
        print("Veuillez saisir un nombre, 1) ou 2)")
        continue
    else:
        print("Veuillez saisir un nombre, 1) ou 2)")
        continue
    # exit()


print("-*-" * 33)
if joueur_pts_de_vie <= 0:
    print("Fin de la partie, vous avez perdu")
elif monstre_pts_de_vie <= 0:
    print("Vous avez gagné !")