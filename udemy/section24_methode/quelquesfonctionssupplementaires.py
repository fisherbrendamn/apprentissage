# Fonction len
# Fonction round pour arrondir à l'entier le plus proche 

print(round(2.8))

# Min et max
liste = [1, 33, 15]
print('La liste : ',liste)
print('Maximum : ',max(liste))
print('Minimum : ',min(liste))

# Sum - somme des éléments dans une liste
print('Somme : ',sum(liste))

# Range
range(5) # Donne une liste de 0 à 4
range(2, 5) # Donne de 2 à 4