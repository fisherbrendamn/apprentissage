mdp = input('Saisissez un nouveau mot de passe : ')
mdp_trop_court = "votre mot de passe est trop court."

# mdp_valide = False
# while mdp_valide == False:
if len(mdp) <= 8:
    print(mdp_trop_court.capitalize())
elif mdp.isdigit():
    print("Votre mot de passe ne peut pas contenir que des chiffres")
else:
    mdp_valide = True
    print('Mot de passe validé.')