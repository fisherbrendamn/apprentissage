import sqlite3
import os

CUR_DIR = os.path.dirname(__file__) # Ce n'est pas la bonne variable, retrouver dans précédents exo
chemin_db = os.path.join(CUR_DIR, "database.db")

print(chemin_db)

conn = sqlite3.connect(chemin_db)

# Curseur
c = conn.cursor()

c.execute("""
CREATE TABLE IF NOT EXISTS employees(
    prenom text,
    nom text,
    salaire int
)
""")

d = {"prenom": "Harald", "nom": "Maldant", "salaire": 24000}

c.execute("INSERT INTO employees VALUES (:prenom, :nom, :salaire)", d)

# c.execute('SELECT * FROM employees WHERE prenom="Harald"', d)
# donnees = c.fetchall()
# for donnee in donnees:
#     print(donnee)

c.execute("""UPDATE employees SET salaire=:salaire WHERE prenom=:prenom AND nom=:nom""", d)

# On ne commit pas avec le curseur mais avec la connexion
conn.commit()
conn.close()