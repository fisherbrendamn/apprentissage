import json

fichier = "/Users/hmaldant/Documents/sandbox/apprentissage/udemy/recup_données/settings.json"

with open(fichier, 'r') as f:
    settings = json.load(f)

# print(settings.get("fontSize"))
# print(type(settings))

settings["fontSize"] = 13

with open(fichier, 'w') as f:
    json.dump(settings, f, indent=4)

